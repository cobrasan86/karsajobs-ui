# !/bin/bash
# Author : Cobrasan86

#Melakukan build docker image dari Dockerfile dan merubah menerapkan tag
docker build -t karsajobs-ui:latest .
docker image tag karsajobs-ui:latest cobrasan86/karsajobs-ui:latest

#Melakukan push image ke Github Packages
docker image push cobrasan86/karsajobs-ui:latest
